﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

namespace GesturesTest {
	public class GUIControl : MonoBehaviour {

		public Text ScoreText;
		public Button StartButton;
		public Text TimeText;
		public Text FinalScoreText;
		public GameObject TargetImagePanel;
		public Image TargetImage;
		public Button StopButton;
		public GameManager GameManagerRef;
		public int LinesWidth = 3;

		public GameObject ImageSuccess;
		public GameObject ImageFail;

		public Image DebugImage;

		public void Start() {
			if (GameManagerRef == null) {
				GameManagerRef = GameObject.FindObjectOfType<GameManager>();
			}
			StartButton.onClick.RemoveAllListeners();
			StartButton.onClick.AddListener(() => {
				GameManagerRef.StartGame();
			});
			StopButton.onClick.RemoveAllListeners();
			StopButton.onClick.AddListener(() => {
				GameManagerRef.EndGame();
			});
			GameManager.OnGameStarted += OnGameStart;
			GameManager.OnGameEnded += OnGameEnd;
			GameManager.OnScoreChanged += OnScoreChanged;
			GameManager.OnTimerChanged += OnTimerChanged;
			GameManager.OnComparison += ComparisonIndicator;
			HideHUD();
			HideIndicators();
			FinalScoreText.gameObject.SetActive(false);
		}

		void OnGameStart() {
			ShowHUD();
		}

		void OnGameEnd() {
			HideHUD();
			FinalScoreText.text = "Your score = " + GameManagerRef.Score.ToString();
			StartButton.GetComponentInChildren<Text>().text = "Restart";
		}

		void OnScoreChanged(int score) {
			ScoreText.text = "Score:" + score;
			BuildPreviewForTargetShape();
			DebugImage.gameObject.SetActive(false);//debug
		}

		void OnTimerChanged(float time) {
			TimeText.text = time.ToString("0.00");
		}

		public void ShowHUD() {
			ScoreText.gameObject.SetActive(true);
			StartButton.gameObject.SetActive(false);
			TimeText.gameObject.SetActive(true);
			FinalScoreText.gameObject.SetActive(false);
			TargetImagePanel.gameObject.SetActive(true);
			StopButton.gameObject.SetActive(true);
		}

		public void HideHUD() {
			ScoreText.gameObject.SetActive(false);
			StartButton.gameObject.SetActive(true);
			TimeText.gameObject.SetActive(false);
			FinalScoreText.gameObject.SetActive(true);
			TargetImagePanel.gameObject.SetActive(false);
			StopButton.gameObject.SetActive(false);
		}

		void ComparisonIndicator(bool success) {
			if (success) {
				ImageSuccess.SetActive(true);
				Invoke("HideIndicators", 0.1f);
			}
			else {
				ImageFail.SetActive(true);
				Invoke("HideIndicators", 0.1f);
			}
		}

		void HideIndicators() {
			ImageSuccess.SetActive(false);
			ImageFail.SetActive(false);
		}

		Sprite BuildSpriteFromShape(Vector2[] shape, Vector2 pixelSize) {
			var tex = new Texture2D((int)pixelSize.x, (int)pixelSize.y, TextureFormat.ARGB32, false);
			for (int i = 0; i < tex.width; i++) {
				for (int j = 0; j < tex.height; j++) {
					tex.SetPixel(i, j, new Color(0, 0, 0, 0));
				}
			}
			tex.Apply();
			if (GameManagerRef.target.Length > 0) {
				shape = PolylinesUtils.NormalizeShapeSize(shape, Mathf.Max(pixelSize.x, pixelSize.y) * 0.6f);
				for (int i = 0; i < shape.Length - 1; i++) {
					DrawLine(tex, shape[i] + pixelSize / 2f, shape[i + 1] + pixelSize / 2f, Color.black);
				}
				tex.Apply();
			}
			return Sprite.Create(tex, new Rect(0, 0, tex.width, tex.height), new Vector2(0.5f, 0.5f));

		}


		void BuildPreviewForTargetShape() {
			TargetImage.sprite = BuildSpriteFromShape(GameManagerRef.target, TargetImage.rectTransform.rect.size);
		}

		public void BuildDebugPreviewForShape(Vector2[] shape) {
			DebugImage.sprite = BuildSpriteFromShape(shape, DebugImage.rectTransform.rect.size);
			DebugImage.gameObject.SetActive(true);
		}

		void DrawLine(Texture2D tex, Vector2 v1, Vector2 v2, Color col) {
			DrawLine(tex, (int)v1.x, (int)v1.y, (int)v2.x, (int)v2.y, LinesWidth, col);
		}

		void DrawLine(Texture2D tex, int x0, int y0, int x1, int y1, int width, Color col) {
			int dy = (int)( y1 - y0 );
			int dx = (int)( x1 - x0 );
			int stepx, stepy;

			if (dy < 0) {
				dy = -dy;
				stepy = -1;
			}
			else {
				stepy = 1;
			}
			if (dx < 0) {
				dx = -dx;
				stepx = -1;
			}
			else {
				stepx = 1;
			}
			dy <<= 1;
			dx <<= 1;

			float fraction = 0;

			//tex.SetPixel(x0, y0, col);
			DrawFilledCircle(tex, x0, y0, width, col);
			if (dx > dy) {
				fraction = dy - ( dx >> 1 );
				while (Mathf.Abs(x0 - x1) > 1) {
					if (fraction >= 0) {
						y0 += stepy;
						fraction -= dx;
					}
					x0 += stepx;
					fraction += dy;
					//tex.SetPixel(x0, y0, col);
					DrawFilledCircle(tex, x0, y0, width, col);
				}
			}
			else {
				fraction = dx - ( dy >> 1 );
				while (Mathf.Abs(y0 - y1) > 1) {
					if (fraction >= 0) {
						x0 += stepx;
						fraction -= dy;
					}
					y0 += stepy;
					fraction += dx;
					//tex.SetPixel(x0, y0, col);
					DrawFilledCircle(tex, x0, y0, width, col);
				}
			}
		}

		public void DrawFilledCircle(Texture2D tex, int cx, int cy, int radius, Color col) {
			int x, y, px, nx, py, ny;

			for (x = 0; x <= radius; x++) {
				int d = (int)Mathf.Ceil(Mathf.Sqrt(( radius ) * ( radius ) - x * x));
				for (y = 0; y <= d; y++) {
					px = cx + x;
					nx = cx - x;
					py = cy + y;
					ny = cy - y;
					if (px >= 0 && px < tex.width && py >= 0 && py < tex.height) {
						tex.SetPixel(px, py, col);
					}
					if (nx >= 0 && nx < tex.width && py >= 0 && py < tex.height) {
						tex.SetPixel(nx, py, col);
					}
					if (px >= 0 && px < tex.width && ny >= 0 && ny < tex.height) {
						tex.SetPixel(px, ny, col);
					}
					if (nx >= 0 && nx < tex.width && ny >= 0 && ny < tex.height) {
						tex.SetPixel(nx, ny, col);
					}

				}
			}
		}
	}
}
