﻿using UnityEngine;
using System.Collections;
using System.Xml;
using System.Collections.Generic;
using System.Globalization;

namespace GesturesTest {

	public static class PolylinesUtils {

		public static Vector2[][] LoadShapesPathes() {
			var files = Resources.LoadAll<TextAsset>("Shapes");

			List<Vector2[]> shapesArrays = new List<Vector2[]>();

			XmlDocument doc = new XmlDocument();
			for (int i = 0; i < files.Length; i++) {
				doc.LoadXml(files[i].text);
				var nodes = doc.SelectNodes("//polyline");
				foreach (XmlNode node in nodes) {
					var shapePoints = new List<Vector2>();
					var strVec = node.InnerText.Split(new char[] { ' ', '\n', '\r','\t'}, System.StringSplitOptions.RemoveEmptyEntries);
					for (int j = 0; j < strVec.Length; j++) {
						Vector2 v;
						if (StringToVector2(strVec[j], out v)) {
							shapePoints.Add(v);
						}
					}
					shapesArrays.Add( CenterizeShape(shapePoints.ToArray()) );
				}
			}
			return shapesArrays.ToArray();
		}

		public static bool StringToVector2(string serializedVector2, out Vector2 result) {
			var floats = serializedVector2.Split(new char[] { ';', '/', '(', ')', '*' });
			int read = 0;
			float x = 0;
			float y = 0;

			for (int i = 0; i < floats.Length; i++) {
				if (read == 0) {
					if (float.TryParse(floats[i], System.Globalization.NumberStyles.Any, CultureInfo.InvariantCulture, out x)) {
						read++;
					}
				}
				else {
					if (float.TryParse(floats[i], System.Globalization.NumberStyles.Any, CultureInfo.InvariantCulture, out y)) {
						read++;
						break;
					}
				}
			}
			result = new Vector2(x, y);
			return read == 2;
		}

		static bool LinesIntersection(Vector2 v1start, Vector2 v1end, Vector2 v2start, Vector2 v2end, out Vector2 intersection) {
			float a1 = v1end.y - v1start.y;
			float b1 = v1start.x - v1end.x;
			float c1 = a1 * v1start.x + b1 * v1start.y;
			float a2 = v2end.y - v2start.y;
			float b2 = v2start.x - v2end.x;
			float c2 = a2 * v2start.x + b2 * v2start.y;
			float det = a1 * b2 - a2 * b1;
			if (Mathf.Approximately(det, 0)) {
				intersection = new Vector2();
				return false;
			}
			var p = new Vector2(
				( b2 * c1 - b1 * c2 ) / det,
				( a1 * c2 - a2 * c1 ) / det);
			var v1lenSqr = ( v1start - v1end ).sqrMagnitude;
			var v2lenSqr = ( v2start - v2end ).sqrMagnitude;
			if (( p - v1start ).sqrMagnitude < v1lenSqr && ( p - v1end ).sqrMagnitude < v1lenSqr) {
				if (( p - v2start ).sqrMagnitude < v2lenSqr && ( p - v2end ).sqrMagnitude < v2lenSqr) {
					intersection = p;
					return true;
				}
			}
			intersection = new Vector2();
			return false;
		}

		static bool LineAndRayIntersection(Vector2 vStart, Vector2 vEnd, Vector2 rayStart, Vector2 rayDirection, out Vector2 intersection) {
			float a1 = vEnd.y - vStart.y;
			float b1 = vStart.x - vEnd.x;
			float c1 = a1 * vStart.x + b1 * vStart.y;
			Vector2 rayEnd = rayStart + rayDirection;
			float a2 = rayEnd.y - rayStart.y;
			float b2 = rayStart.x - rayEnd.x;
			float c2 = a2 * rayStart.x + b2 * rayStart.y;
			float det = a1 * b2 - a2 * b1;
			if (Mathf.Approximately(det, 0)) {
				intersection = new Vector2();
				return false;
			}
			var p = new Vector2(
				( b2 * c1 - b1 * c2 ) / det,
				( a1 * c2 - a2 * c1 ) / det);
			var v1lenSqr = ( vStart - vEnd ).sqrMagnitude;
			if (( p - vStart ).sqrMagnitude < v1lenSqr && ( p - vEnd ).sqrMagnitude < v1lenSqr) {
				intersection = p;
				return true;
			}
			intersection = new Vector2();
			return true;
		}

		public static Vector2[] CenterizeShape(Vector2[] p) {
			if (p != null && p.Length > 0) {
				Vector2 center = new Vector2();
				for (int i = 0; i < p.Length; i++) {
					center += p[i];
				}
				center = center / p.Length;
				for (int i = 0; i < p.Length; i++) {
					p[i] -= center;
				}
			}
			return p;
		}

		public static Vector2 GetShapeSize(Vector2[] shape) {
			if (shape.Length > 0) {
				Vector2 min = shape[0];
				Vector2 max = shape[0];
				for (int i = 1; i < shape.Length; i++) {
					if (shape[i].x < min.x) {
						min.x = shape[i].x;
					}
					if (shape[i].y < min.y) {
						min.y = shape[i].y;
					}
					if (shape[i].x > max.x) {
						max.x = shape[i].x;
					}
					if (shape[i].y > max.y) {
						max.y = shape[i].y;
					}
				}
				return new Vector2(Mathf.Abs(max.x - min.x), Mathf.Abs(max.y - min.y));
			}
			return new Vector2();
		}

		public static Vector2[] NormalizeShapeSize(Vector2[] shape, float sizeMlt) {
			if (sizeMlt > 0) {
				var shapeSize = GetShapeSize(shape);
				var average = ( shapeSize.x + shapeSize.y ) / 2f;
				if (average > 0) {
					sizeMlt = sizeMlt / average;
					for (int i = 0; i < shape.Length; i++) {
						shape[i] = shape[i] * sizeMlt;
					}
				}
			}
			return shape;
		}

		public static Vector2[] GetPointsIntersectedWithRay(Vector2[] shape, Vector2 rayOrigin, Vector2 rayDirection) {
			List<Vector2> result = new List<Vector2>();
			for (int i = 0; i < shape.Length - 1; i++) {
				Vector2 v;
				if (LinesIntersection(shape[i], shape[i + 1], rayOrigin, rayOrigin + rayDirection * 100f, out v)) {
					result.Add(v);
				}
			}
			return result.ToArray();
		}

		public static bool ArePolylinesMatch(Vector2[] etalon, Vector2[] testShape, float dispersion, int scanRate) {
			etalon = CenterizeShape(etalon);
			testShape = CenterizeShape(testShape);
			var etalonSizeV = GetShapeSize(etalon);
			var etalonSize = ( etalonSizeV.x + etalonSizeV.y ) / 2f;
			testShape = NormalizeShapeSize(testShape, etalonSize);
			//====DEBUG
			GameObject.FindObjectOfType<GUIControl>().BuildDebugPreviewForShape(testShape);
			//====
			var sqrDispersion = Mathf.Pow(etalonSize * dispersion / 100f, 2f);
			for (int i = 0; i < scanRate; i++) {
				float angle = Mathf.PI * 2f * ( i / ( scanRate - 1 ) );
				var direction = new Vector2(Mathf.Sin(angle), Mathf.Cos(angle));
				var etalonPoints = GetPointsIntersectedWithRay(etalon, new Vector2(), direction);
				var testPoints = GetPointsIntersectedWithRay(testShape, new Vector2(), direction);
				for (int j = 0; j < etalonPoints.Length; j++) {
					if (testPoints.Length > j) {
						if (( testPoints[j] - etalonPoints[j] ).sqrMagnitude > sqrDispersion) {
							return false;
						}
					}
					else {
						return false;
					}
				}
			}
			return true;
		}
	}
}
