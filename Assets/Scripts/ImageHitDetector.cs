﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;

namespace GesturesTest {
	public class ImageHitDetector : MonoBehaviour, IDragHandler, IPointerUpHandler, IEndDragHandler {

		/// <summary>
		/// local screenspace pos, world pos
		/// </summary>
		public static event System.Action<Vector2, Vector3> OnTouchDragEvent;
		public static event System.Action OnTouchUpEvent;
		public static event System.Action OnEndDragEvent;

		public void OnDrag(PointerEventData eventData) {
			if (OnTouchDragEvent != null) {
				var worldPos = eventData.pressEventCamera.ScreenToWorldPoint(eventData.position); //eventData.pointerCurrentRaycast.worldPosition;
				worldPos.z = transform.position.z;
				OnTouchDragEvent(eventData.position, worldPos);
			}
		}

		public void OnPointerUp(PointerEventData eventData) {
			if (OnTouchUpEvent != null) {
				OnTouchUpEvent();
			}
		}

		public void OnEndDrag(PointerEventData eventData) {
			if (OnEndDragEvent != null) {
				OnEndDragEvent();
			}
		}
	}
}
