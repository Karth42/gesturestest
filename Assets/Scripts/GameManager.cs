﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace GesturesTest {
	public class GameManager : MonoBehaviour {

		public static event System.Action<int> OnScoreChanged;
		public static event System.Action OnGameStarted;
		public static event System.Action OnGameEnded;
		public static event System.Action<float> OnTimerChanged;
		public static event System.Action<bool> OnComparison;

		public Transform TrailTransRef;
		public TrailRenderer TrailRendRef;
		public bool IsGameStarget;
		public List<Vector2> PointsBuffer = new List<Vector2>();
		float _lastTime;
		public float MinimalDeltaTimeForInput = 0.1f;

		[Header("Template comparison parameters:")]
		public float Dispersion = 5f;
		public int RadialScanSectors = 60;

		[Header("Timer parameters:")]
		[Tooltip("Time for first level")]
		public float TimeAtBegin = 50f;
		[Tooltip("Time at maximum level")]
		public float TimeMinimal = 10f;
		[Tooltip("Maximal level for time function")]
		public int TimeFromMaxToMinLevels = 45;

		private int _score = -1;
		public int Score {
			get {
				return _score;
			}
			set {
				if (_score != value) {
					_score = value;
					if (OnScoreChanged != null) {
						OnScoreChanged(value);
					}
				}
			}
		}

		private int _level = -1;
		public int Level {
			get {
				return _level;
			}
		}

		private float _timer = 0;
		public float Timer {
			get {
				return _timer;
			}
			set {
				if (_timer != value) {
					_timer = value;
					if (OnTimerChanged != null) {
						OnTimerChanged(_timer);
					}
				}
			}
		}

		Vector2[][] targets;

		public Vector2[] target {
			get {
				if (_level >=0) {
					return targets[_level % targets.Length];
				}
				else {
					return new Vector2[0];
				}
			}
		}

		void Start() {
			if (TrailTransRef == null) {
				TrailRendRef = GameObject.FindObjectOfType<TrailRenderer>();
				if (TrailRendRef != null) {
					TrailTransRef = TrailRendRef.transform;
				}
			}
			if (TrailRendRef == null) {
				TrailTransRef.GetComponent<TrailRenderer>();
			}
			targets = PolylinesUtils.LoadShapesPathes();
		}

		void Update() {
			if (IsGameStarget) {
				Timer -= Time.deltaTime;
				if (Timer <= 0) {
					EndGame();
				}
			}
		}

		public void StartGame() {
			IsGameStarget = true;
			SubscribeInput();
			if (OnGameStarted != null) {
				OnGameStarted();
			}
			_level = 0;
			Score = 0;
			Timer = GetTimeForLevel(0);
		}

		public void EndGame() {
			IsGameStarget = false;
			TrailRendRef.Clear();
			UnsubscribeInput();
			if (OnGameEnded != null) {
				OnGameEnded();
			}
		}

		public float GetTimeForLevel(int level) {
			//график этой функции сначала снижается медленно, потом быстро и в конце снова медленно
			return TimeAtBegin - ( TimeAtBegin - TimeMinimal ) * Mathf.SmoothStep(0f, 1f, Mathf.Clamp(level / (float)TimeFromMaxToMinLevels, 0f, 1f));
		}

		void SubscribeInput() {
			UnsubscribeInput();
			ImageHitDetector.OnTouchDragEvent += OnDrag;
			ImageHitDetector.OnEndDragEvent += OnDragEnd;
		}

		void UnsubscribeInput() {
			ImageHitDetector.OnTouchDragEvent -= OnDrag;
			ImageHitDetector.OnEndDragEvent -= OnDragEnd;
		}

		void OnDrag(Vector2 localSpace, Vector3 worldSpace) {
			TrailTransRef.position = worldSpace;
			if (Time.time > _lastTime + MinimalDeltaTimeForInput) {
				_lastTime = Time.time;
				PointsBuffer.Add(localSpace);
				//DebugShowPoints();
			}
		}

		void OnDragEnd() {
			CompareShapes();
			TrailRendRef.Clear();
			PointsBuffer.Clear();
		}

		void CompareShapes() {
			bool same = PolylinesUtils.ArePolylinesMatch(target, PointsBuffer.ToArray(), Dispersion, RadialScanSectors);
			if (same) {
				_level++;
				Score++;
				Timer = GetTimeForLevel(_level);
			}
			if (OnComparison != null) {
				OnComparison(same);
			}
		}
	}
}